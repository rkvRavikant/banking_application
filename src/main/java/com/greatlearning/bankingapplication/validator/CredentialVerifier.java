package com.greatlearning.bankingapplication.validator;

import java.util.Optional;

import com.greatlearning.bankingapplication.datastore.Data;
import com.greatlearning.bankingapplication.pojo.Customer;

public class CredentialVerifier {

	/**
	 * validate method validates the account credential 
	 * @param accountNo
	 * @param pwd
	 * @return the validation result
	 */
	public boolean validateCredential(final String accountNo, final String pwd) {
		Optional<Customer> customerMatch = Data.customers.stream().filter(customerDetail -> customerDetail.getBankAccountNo().equals(accountNo)
				&& customerDetail.getPassword().equals(pwd)).findFirst();
		return (customerMatch.isEmpty())? false : true;
	}
}
