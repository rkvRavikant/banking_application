package com.greatlearning.bankingapplication.util;

import java.util.Scanner;

public class OTPManager {

	
	/**
	 * generateOtp- generates 4 digit random otp.
	 * @return generated otp
	 */
	public int generateOtp() {
		return (int) (Math.random()*9000)+1000;
	}
	
private static int attempt = 1;
	
	/**
	 * validateOtp method takes the expected and acual otp to validate, it tries with 3 invalid otps after that it fails.
	 * @param actualOtp
	 * @param expectedOtp
	 * @return result of otp validation
	 */
	public boolean validateOtp(int actualOtp, int expectedOtp) {
		if(actualOtp == expectedOtp)
			return true;
        else {
            attempt++;
            if(attempt == 4) {
        		System.out.println("Multiple Invalid OTP attempted");
        		return false;
        	}
            System.out.println("Invalid OTP. please enter valid otp");
            Scanner sc = new Scanner(System.in);
            validateOtp(actualOtp, sc.nextInt());
        }
		return false;
	}

}
