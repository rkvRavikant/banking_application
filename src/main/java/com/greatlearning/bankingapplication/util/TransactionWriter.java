package com.greatlearning.bankingapplication.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TransactionWriter {
	
	/**
	 * writeTransactions method stores the transaction details in the transactions.txt file.
	 * @param transaction- transaction details which need to be stored
	 */
	public void writeTransactions(String transaction) {
		try {
			FileWriter fileWriter = new FileWriter("transactions.txt", true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(transaction+"\n");
			bufferedWriter.close();
		} catch (IOException e) {
			System.err.println("Exception while writing the transactions");
		}
	}

}
