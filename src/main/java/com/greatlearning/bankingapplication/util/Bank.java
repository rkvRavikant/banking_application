package com.greatlearning.bankingapplication.util;

import java.util.Scanner;


public class Bank {
	
	OTPManager otpManager = new OTPManager();
	TransactionWriter transactionWriter = new TransactionWriter();
	/**
	 * operate method performs all the banking operation.
	 * @param sc scanner to read the input
	 */
	public void operate(Scanner sc) {
        System.out.println("------------------------------------------------");
        System.out.println("Enter the operation that you want to perform");
        System.out.println("1.  Deposit");
        System.out.println("2.  Withdrawl");
        System.out.println("3.  Transfer");
        System.out.println("0.  Logout");
        System.out.println("------------------------------------------------");
        int operation = sc.nextInt();
        switch (operation) {
            case 1: {
                System.out.println("enter the amount you want to deposit");
                int amount = sc.nextInt();
                System.out.println("amount " + amount + " deposit successfully");
                transactionWriter.writeTransactions("amount deposit transaction");
                break;

            }
            case 2: {
                System.out.println("enter the amount you want to withdrawl");
                int amount = sc.nextInt();
                System.out.println("amount " + amount + " withdrawl successfully");
                transactionWriter.writeTransactions("amount withdrawl transaction");
                break;

            }
            case 3: {
                System.out.println("Enter the otp");
                int actualOtp = otpManager.generateOtp();
                System.out.println(actualOtp);
                int expectedOtp = sc.nextInt();
                boolean valid = otpManager.validateOtp(actualOtp, expectedOtp);
                if(!valid) return;
                System.out.println("otp verification Successful !!!");
                System.out.println("enter the amount and BankAccount no to which you want to transfer");
                int amount = sc.nextInt();
                int bankNo = sc.nextInt();
                System.out.println("amount " + amount + " transferred successfully to bankAccount " + bankNo+"\n");
                transactionWriter.writeTransactions("amount transfer transaction");
                break;

            }
            case 0:
            	System.out.println("Exited successfully");
                break;
            default: {
            	operate(sc);
            }

        }

    }

}
