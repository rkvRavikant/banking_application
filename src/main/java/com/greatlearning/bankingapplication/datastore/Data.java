package com.greatlearning.bankingapplication.datastore;

import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bankingapplication.pojo.Customer;

public class Data {

	//list to hold the predefined customers for validation
	public static List<Customer> customers= new ArrayList<Customer>();

	// static block to initialize and load the customers in the customers list. 
	static {
		customers.add(new Customer("32390016", "password1"));
		customers.add(new Customer("32390018", "password2"));
	}
	
	
}
