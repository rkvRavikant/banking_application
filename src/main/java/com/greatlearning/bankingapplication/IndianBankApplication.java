package com.greatlearning.bankingapplication;

import java.util.Scanner;

import com.greatlearning.bankingapplication.util.Bank;
import com.greatlearning.bankingapplication.validator.CredentialVerifier;

public class IndianBankApplication {
	
	public static void main(String[] args) {
		CredentialVerifier credentialVerifier = new CredentialVerifier();
		Bank bank = new Bank();
		System.out.println("welcome to login page");
		System.out.println("enter the bank Account no");
		Scanner sc = new Scanner(System.in);
        String accountNumber = sc.next();
        System.out.println("enter the password for the corresponding bank account no");
        String password = sc.next();
        boolean valid = credentialVerifier.validateCredential(accountNumber, password);
        if (valid) {
            System.out.println("!!!!! Welcome to Indian Bank !!!!!");
            bank.operate(sc);
            
        } else 
            System.out.println("Invalid credentials");
	}

}
